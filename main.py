import os
import glob
import shutil
import hashlib 
import time
from pprint import pprint

import requests
import mysql.connector
from selenium import webdriver
from selenium.webdriver.common.keys import Keys  
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup


HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'
}

IMAGE_PATH = os.path.join(os.getcwd(),'images')
CONTENTS_PATH = os.getenv("CONTENTS_PATH", os.path.dirname(os.path.join(os.getcwd(), "contents")))

DATABASE_HOST = os.getenv("DATABASE_HOST", "localhost")
DATABASE_USER = os.getenv("DATABASE_USER", "root")
DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD", ")Go7+G[OGka{VlZd=k)%")
DATABASE_NAME = os.getenv("DATABASE_NAME", "db_start1")
DEBUG = os.getenv("DEBUG", True)


class Scraper:

    count = 0
    total = 0
    contents = []
    connection = None
    cursor = None

    download_pictures = True

    def __init__(self, download_pictures=True):
        self.total = self.get_contents_count()
        self.connection = self.connection()
        self.cursor = self.connection.cursor(buffered=True)
        self.download_pictures = download_pictures

    def download_contents_from_file(self, filename="output.txt"):
        file = open(filename, "r") 
        lines = file.readlines()
        os.makedirs(os.path.join(CONTENTS_PATH, "contents"), exist_ok=True)    
        for line in lines:
            md5 = hashlib.md5(line.encode()).hexdigest()
            filename =  f"{md5}.html"
            file_path = os.path.join(CONTENTS_PATH, "contents", filename)
            if not os.path.exists(file_path):
                chrome_options = Options()  
                chrome_options.add_argument("--headless")  
                driver = webdriver.Chrome(chrome_options=chrome_options)
                driver.get(line)
                html = driver.page_source
                open(file_path, "w").write(html)

    def connection(self):
        return mysql.connector.connect(
            host=DATABASE_HOST,
            user=DATABASE_USER,
            password=DATABASE_PASSWORD,
            database=DATABASE_NAME
        )

    def get_contents_count(self):
        count = len(glob.glob1(os.path.join(CONTENTS_PATH, "contents"),"*.html"))
        return count

    def get_contents(self):
        os.chdir(os.path.join(CONTENTS_PATH, "contents"))

        for file in glob.glob("*.html"):
           with open(file, encoding="utf8") as html_file:
                 yield html_file

    def get_filename_from_content(self, content):
        return os.path.basename(content.name)

    def get_html_from_content(self, content):
        return content.read()

    def get_soup_from_content(self, content):
        soup = BeautifulSoup(content, "html.parser")
        return soup

    def get_breadcrumps_from_soup(self, soup):
        return [
            item.find("a").text.strip() for item in soup.find_all("uc-breadcrumbs-link")
        ]

    def get_pictures_from_soup(self, soup):
        return [picture.get("srcset") for picture in soup.find("picture").find_all("source")]

    def get_price_from_soup(self, soup):
        price = soup.find("uc-plp-item-price")
        if price:
            return price.text.strip()

    def get_title_from_soup(self, soup):
        title = soup.find("h1")
        if title:
            return title.text.strip()    

    def get_description_from_soup(self, soup):
        description = soup.find("uc-pdp-section-vlimited").find("div").find("p")
        if description:
            return description.text.strip()

    def get_characteristics_from_soup(self, soup):
        characteristics = []
        for item in soup.find_all("div", class_="def-list__group"):
            term = item.find("dt", class_="def-list__term").text.strip()
            definition = item.find("dd", class_="def-list__definition").text.strip()
            characteristics.append({ term: definition })
        return characteristics

    def download_pictures_from_urls(self, urls, folder):
        attempts = 0
        paths = []
        for url in urls:
            filename = url.split('/')[-1]
            directory = os.path.join(IMAGE_PATH, folder)
            path = os.path.join(directory, filename)

            response = requests.get(url,headers=HEADERS, stream=True, timeout=5)
            if response.status_code == 200:
                os.makedirs(directory, exist_ok=True)
                if not os.path.exists(path):
                    with open(path,'wb') as f:
                        paths.append(path)
                        response.raw.decode_content = True
                        shutil.copyfileobj(response.raw,f)
        return paths

    def save_to_db(self, breadcrumps, pictures, price, title, description, characteristics, filename):
        data = {
            "breadcrumps": breadcrumps,
            "pictures": pictures,
            "price": price,
            "title": title,
            "description": description,
            "characteristics": characteristics,
            "filename": filename
        }
        sql = "SELECT * FROM db_start1.b_iblock_property WHERE NAME = '%s';" % (breadcrumps[-1], )   
        query = self.cursor.execute(sql)

    def progress(self):
        self.count += 1
        self.printProgressBar(
            self.count,
            self.total,
            prefix="Progress:",
            suffix="Complete",
            length=50
        )

    def printProgressBar(self, iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = "█", printEnd = "\r"):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
        if iteration == total: 
            print()

    def run(self):
        if next(self.get_contents(), None) is None:
            self.connection.close()

        for content in self.get_contents():
            soup = self.get_soup_from_content(content)
            filename = self.get_filename_from_content(content)
            breadcrumps = self.get_breadcrumps_from_soup(soup)
            pictures = self.get_pictures_from_soup(soup)
            if self.download_pictures:
                downloaded_pictures_paths = self.download_pictures_from_urls(pictures, filename.split(".")[0])
                pictures = downloaded_pictures_paths
            price = self.get_price_from_soup(soup)
            title = self.get_title_from_soup(soup)
            description = self.get_description_from_soup(soup)
            characteristics = self.get_characteristics_from_soup(soup)
            self.save_to_db(breadcrumps, pictures, price, title, description, characteristics, filename)
            self.progress()


if __name__ == "__main__":
    scraper = Scraper(download_pictures=False)
    scraper.run()
